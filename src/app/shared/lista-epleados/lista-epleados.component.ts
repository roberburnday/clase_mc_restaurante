import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-epleados',
  templateUrl: './lista-epleados.component.html',
  styleUrls: ['./lista-epleados.component.scss']
})
export class ListaEpleadosComponent implements OnInit {

  @Input() empleados:Array<string> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
