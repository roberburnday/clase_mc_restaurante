import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEpleadosComponent } from './lista-epleados.component';

describe('ListaEpleadosComponent', () => {
  let component: ListaEpleadosComponent;
  let fixture: ComponentFixture<ListaEpleadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaEpleadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEpleadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
