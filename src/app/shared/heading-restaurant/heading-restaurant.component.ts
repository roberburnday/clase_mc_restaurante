import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-heading-restaurant',
  templateUrl: './heading-restaurant.component.html',
  styleUrls: ['./heading-restaurant.component.scss']
})
export class HeadingRestaurantComponent implements OnInit {

  @Input() nombre = "";
  @Input() direccion = "";

  constructor() { }

  ngOnInit(): void {
  }

}
