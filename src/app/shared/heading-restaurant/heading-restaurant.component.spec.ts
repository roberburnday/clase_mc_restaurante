import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadingRestaurantComponent } from './heading-restaurant.component';

describe('HeadingRestaurantComponent', () => {
  let component: HeadingRestaurantComponent;
  let fixture: ComponentFixture<HeadingRestaurantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeadingRestaurantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadingRestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
