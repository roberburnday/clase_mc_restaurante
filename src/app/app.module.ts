import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RestuarantUIComponent } from './views/restuarant-ui/restuarant-ui.component';
import { HeadingRestaurantComponent } from './shared/heading-restaurant/heading-restaurant.component';
import { ListaEpleadosComponent } from './shared/lista-epleados/lista-epleados.component';
import { ListaPedidosComponent } from './shared/lista-pedidos/lista-pedidos.component';

@NgModule({
  declarations: [
    AppComponent,
    RestuarantUIComponent,
    HeadingRestaurantComponent,
    ListaEpleadosComponent,
    ListaPedidosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
