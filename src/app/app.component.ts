import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  nombre = "Casa Pepe";
  direccion = "C/ Falsa 123, Pueblo inventado, Albacete";

  empleados = ['Jose Miguel', 'Rosaura Manolita de los Robles', 'Miguel de Dos Rios', 'Empleado Temoral'];
  pedidos = ['mouse limón - 1', 'cupcakes - 3', 'pollo - 1'];
}
