import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestuarantUIComponent } from './restuarant-ui.component';

describe('RestuarantUIComponent', () => {
  let component: RestuarantUIComponent;
  let fixture: ComponentFixture<RestuarantUIComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestuarantUIComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestuarantUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
